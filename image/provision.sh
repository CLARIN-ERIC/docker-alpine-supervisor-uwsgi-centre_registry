#!/bin/bash
set -e

WORK_PATH="/srv"
SETTINGS_PATH="${WORK_PATH}/centre_registry_project/settings.py"
DATA_PROVISION_PATH="/centre_registry_config.d"
PROVISION_CONFIG_PATH="${DATA_PROVISION_PATH}/Centre-Registry-config"
PROVISION_DATABASE_PATH="${DATA_PROVISION_PATH}/Centre-Registry-database"

cd -- "${WORK_PATH}"

if ! [ -d "${WORK_PATH}/database" ]; then
    mkdir -p "${WORK_PATH}/database"
fi 

# Override default configuration
if [ ! -f "${PROVISION_CONFIG_PATH}/settings.py.tmpl" ]; then
    echo "Centre Registry configuration template not supplied. Using source default."
else
    sigil -f "${PROVISION_CONFIG_PATH}/settings.py.tmpl" \
        debug="${DJANGO_DEBUG:?}" \
        db_secret_key="${DJANGO_DB_SECRET_KEY:?}" \
        piwik_website_id="${DJANGO_PIWIK_WEBSITE_ID:?}" \
        servername="${_SERVERNAME:?}" > "${SETTINGS_PATH}"
fi

# If no data provided (1) load fixtures as initial values, otherwise (0)
DB_FROM_SCRATCH=1

# Initialize database
if [ -d "${PROVISION_DATABASE_PATH}" ]; then    
    if [ "$(find ${PROVISION_DATABASE_PATH} -name \*.sqlite -type f  | wc -l)" -gt 0 ]; then
        echo -n "Trying to copy supplied database files... "
        # Check if db target location exists
        if [ "$(find ${WORK_PATH}/database/ -name database.sqlite -type f  | wc -l)" -gt 0 ]; then
            DB_FROM_SCRATCH=0
            echo "There is already a database file installed! Leaving it intact."
        else
            # Copy supplied database on fresh start
            cp "$(unset -v latest; for file in "${PROVISION_DATABASE_PATH}"/*.sqlite; do [ ! -L "$file" ] && [[ "$file" -nt "$latest" ]] &&  latest=$file; done; echo  "$latest")" "${WORK_PATH}/database/database.sqlite"
            chown -R wsgi:wsgi "${WORK_PATH}"/database
            chmod 0600 "${WORK_PATH}"/database
            DB_FROM_SCRATCH=0
            echo "Done."
	    fi
    fi
fi

# Migrate to target migration if needed
# Migrations should have been committed to Git repo for Centre Registry app!
python3 -m "django" migrate

# If no data source been spotted (backup, persisting volume data) load fixtures
# as initial data

if [ ${DB_FROM_SCRATCH} -eq 1 ]; then
    echo "WARNING: Initializing container from scratch, loading source's test fixtures."
    python3 -m "django" loaddata "${WORK_PATH}/centre_registry/fixtures/test_data.json"
fi

python3 -m "django" collectstatic --clear --no-input
